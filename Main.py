# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 11:29:04 2018

@author: Henningsson, Fredriksson
"""

import PlaneStress as ps

if __name__ == "__main__":
    inputData = ps.InputData()
    inputData.save("data")
    inputData.load("data")  
    outputData = ps.OutputData()
    solver = ps.Solver(inputData,outputData)
    solver.execute()
    report = ps.Report(inputData,outputData)
    print(report)
