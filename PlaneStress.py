# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 14:05:25 2018

@author: Henningsson, Fredriksson
"""

import numpy as np
import calfem.core as cfc
import json
import calfem.geometry as cfg  # <-- Geometrirutiner
import calfem.mesh as cfm      # <-- Nätgenerering
import calfem.vis as cfv       # <-- Visualisering
import calfem.utils as cfu     # <-- Blandade rutiner

class InputData(object):
    
    def __init__(self):
        """Klass för att definiera indata för vår modell."""
        self.h = None
        self.w = None
        self.a = None
        self.b = None
        
        self.E = None
        self.v = None
        self.t = None
        
        self.q = None
        
        self.elSizeFactor = None

        
    def save(self, filename):
        """Spara indata till fil."""

        inputData = {}
        
        inputData["h"] = self.h
        inputData["w"] = self.w
        inputData["a"] = self.a
        inputData["b"] = self.b
        
        inputData["E"] = self.E
        inputData["v"] = self.v
        inputData["t"] = self.t
        
        inputData["q"] = self.q
        
        inputData["elSizeFactor"] = self.elSizeFactor

        ofile = open(filename, "w")
        json.dump(inputData, ofile, sort_keys = True, indent = 4)
        ofile.close()

    def load(self, filename):
        """Läs indata från fil."""

        ifile = open(filename, "r")
        inputData = json.load(ifile)
        ifile.close()
        
        self.h = inputData["h"]
        self.w = inputData["w"]
        self.a = inputData["a"]
        self.b = inputData["b"]
        
        self.E = inputData["E"]
        self.v = inputData["v"]
        self.t = inputData["t"]
        
        self.q = inputData["q"]
        
        self.elSizeFactor = inputData["elSizeFactor"]
        
    def geometry(self):
        """Skapa en geometri instans baserat på definierade parametrar"""

        # --- Skapa en geometri-instans för att lagra vår
        #     geometribeskrivning

        g = cfg.Geometry()

        # --- Enklare att använda referenser till self.xxx

        h = self.h
        w = self.w
        a = self.a
        b = self.b
        

        # --- Punkter i modellen skapas med point(...) metoden

        g.point([0, 0])
        g.point([0, h])
        g.point([(w-a)/2, h])
        g.point([(w-a)/2, h-b])
        g.point([(w+a)/2, h-b])
        g.point([(w+a)/2, h])
        g.point([w, h])
        g.point([w, 0])
        g.point([(w+a)/2, 0])
        g.point([(w+a)/2, b])
        g.point([(w-a)/2, b])
        g.point([(w-a)/2, 0])


        # --- Linjer och kurvor skapas med spline(...) metoden

        g.spline([0, 1], marker = 10)            
        g.spline([1, 2])           
        g.spline([2, 3])
        g.spline([3, 4])            
        g.spline([4, 5])
        g.spline([5, 6])            
        g.spline([6, 7], marker = 76)
        g.spline([7, 8])            
        g.spline([8, 9])
        g.spline([9, 10])            
        g.spline([10, 11])
        g.spline([11, 0])            

        # --- Ytan på vilket nätet skall genereras definieras med 
        #     surface(...) metoden.

        g.surface([0,1,2,3,4,5,6,7,8,9,10,11])

        # --- Slutligen returnerar vi den skapade geometrin

        return g
               
class Solver(object):

    """Klass för att hantera lösningen av vår beräkningsmodell."""
    def __init__(self, inputData, outputData):
        self.inputData = inputData
        self.outputData = outputData
        
    def execute(self):
        """Metod för att utföra finita element beräkningen."""

        # --- Överför modell variabler till lokala referenser

        ep = [1, self.inputData.t]
        E = self.inputData.E
        v = self.inputData.v
        q = self.inputData.q
        elSizeFactor = self.inputData.elSizeFactor

        # --- Anropa InputData för en geomtetribeskrivning

        geometry = self.inputData.geometry()

        # --- Nätgenerering

        meshGen = cfm.GmshMeshGenerator(geometry)
        meshGen.elSizeFactor = elSizeFactor     # <-- Anger max area för element
        meshGen.elType = 2
        meshGen.dofsPerNode = 2
        meshGen.returnBoundaryElements = True

        coord, edof, dof, bdofs, elementmarkers, boundaryElements = meshGen.create()
        
        ndof = len(coord)* meshGen.dofsPerNode
        
        # --- Beräkningskod
        
        K = np.zeros((ndof,ndof))
        D = cfc.hooke(ep[0],E,v)
        nelm = np.shape(edof)[0]
        Ex , Ey = cfc.coordxtr(edof, coord, dof)
			
        for i in range(0,nelm):
            Ke = cfc.plante(Ex[i,:],Ey[i,:],ep,D)
            cfc.assem(edof[i,:],K,Ke)
            
        bc = np.array([],'i')
        bcVal = np.array([],'f')
        
        bc, bcVal = cfu.applybc(bdofs, bc, bcVal, 10, 0.0, 0)
        bcs = np.concatenate((bc,bcVal), axis = 1)
        
        loads = np.zeros((ndof,1))
        cfu.applyforcetotal(bdofs,loads,76,q,1)
        
        a, r = cfc.solveq(K,loads,bcs)    
            
        # --- Överför modell variabler till lokala referenser

        self.outputData.displacements = a
        self.outputData.reactionForces = r
        self.coord = coord
        self.edof = edof
        self.dof = dof
        self.bcs = bcs
        self.loads = loads

class OutputData(object):
    
    def __init__(self):
        self.displacements = None
        self.reactionForces = None
        self.coord = None
        self.edof = None
        self.dof = None
        self.bcs = None
        self.loads = None
        
        
class Report(object):
    
    """Klass för presentation av indata och utdata i rapportform."""
    
    def __init__(self, inputData, outputData):
        self.inputData = inputData
        self.outputData = outputData
        self.report = ""

    def clear(self):
        self.report = ""

    def addText(self, text=""):
        self.report+=str(text)+"\n"

    def __str__(self):
        self.clear()
        self.addText()
        self.addText("-------------- Model input ----------------------------------")
        self.addText()
        self.addText("P L A N E  S T R E S S")
        self.addText()
        self.addText("Thickness:")
        self.addText()
        self.addText(self.inputData.t)
        self.addText()
        self.addText("Young's Modulus:")
        self.addText()
        self.addText(self.inputData.E)
        self.addText()
        self.addText("Poission's Ratio:")
        self.addText()
        self.addText(self.inputData.v)
        self.addText()
        self.addText("Coordinates:")
        self.addText()
        self.addText(self.inputData.coord)
        self.addText()
        self.addText("Edof:")
        self.addText()
        self.addText(self.inputData.edof)
        self.addText()
        self.addText("Dof:")
        self.addText()
        self.addText(self.inputData.dof)
        self.addText()
        self.addText("Loads:")
        self.addText()
        self.addText(self.inputData.loads)
        self.addText()
        self.addText("Boundary Conditions:")
        self.addText()
        self.addText(self.inputData.bcs)
        self.addText()
        self.addText("-------------- RESULTS ----------------------------------")
        self.addText()
        self.addText("Displacements:")
        self.addText()
        self.addText(self.outputData.displacements)
        self.addText()
        self.addText("Reaction Forces:")
        self.addText()
        self.addText(self.outputData.reactionForces)
        self.addText()
        return self.report
